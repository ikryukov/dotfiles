
" Basic options {{{
set nocompatible
filetype plugin on

" Editing
set autoindent                              " Keep indentation after <CR> 
set backspace=indent,eol,start              " Backspace works across newlines
set complete-=i                             " Auto-completion does not scan headers

" UI
set laststatus=2                            " Last window always has status line
set cmdheight=2                             " Larger command split
set wildmenu                                " Auto-completion in menu
set wildignorecase                          " Wildmenu ignores case
set wildmode=longest,full
color slate

if has('nvim')
    set fcs=eob:\
endif

set autoread                                " Automatically reload from disk
set hidden                                  " Don't ask me to save files before switching
set ignorecase                              " Case insensitive search
set smartcase                               " But actually case sensitivity only with caps
set showcmd                                 " Show me what I am typing
set incsearch                               " Highlight search matches
" hlsearch is on by default in neovim
set lazyredraw                              " Don't redraw on macros
set scrolloff=3                             " Keep some padding
set undofile                                " Save that sweet undo history to disk
set undodir=$HOME/.vim_undo_files           " Keep em in one place
set undolevels=2000
" Nice statusline
set statusline=%#StatusLine#\ %n\ %f\ %(%m\[%W%R%Y%q\]\ %)%=
set statusline+=%#LineNr#\ %(%l,%c%V\ %=\ %P\ %)

" set number                                  " Show current line number
" set relativenumber                          " Relative line numbers
set foldmethod=marker                       " Folding with markers
set path=**                                 " Fuzzy matching
set mouse=a                                 " Sometimes I mouse
set spelllang=en_us
set listchars=tab:▸\ ,eol:¬

" no auto comments
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" Don't wait too long
set timeout timeoutlen=1000 ttimeout ttimeoutlen=100

" tabs
set tabstop=4                               " Tabs will look like they are 4 chars wide
set shiftwidth=4                            " New indents have a width of 4
set softtabstop=4                           " Number of columns in a tab
set expandtab                               " Expand tabs to spaces
set smarttab                                " Respect shift sizes
" }}}

" Bindings {{{
nnoremap j gj
nnoremap k gk
set whichwrap+=<,>,h,l,[,]

let mapleader = "\<Space>"
let maplocalleader = ","

nnoremap <leader>cv :tabe ~/.config/nvim/init.vim<CR>
nnoremap <leader>ct :tabe ~/.tmux.conf<CR>
nnoremap <leader>cf :tabe ~/.config/fish/config.fish<CR>

" Align paragraph
nnoremap <silent> Q gwip

" Correct typo
nnoremap <leader>c 1z=

" fold current paragraph
nnoremap <leader>z }o<esc>{o<esc>v}zf

nnoremap <silent> <BS> :nohlsearch<CR>

iabbr <expr> ST! strftime("%c")
iabbr <expr> SD! strftime("%a %e %B %Y (%H:%M)")
" }}}

" Language-specific things {{{
augroup r_custom
    autocmd!
    autocmd FileType r setlocal commentstring=#\ %s
    " autocmd FileType r setlocal foldmethod=syntax
augroup END

augroup cpp_custom
    autocmd!
    autocmd FileType cpp setlocal commentstring=//\ %s
augroup END

augroup c_custom
    autocmd!
    autocmd FileType c setlocal commentstring=//\ %s
augroup END

autocmd FileType markdown setlocal commentstring=<!--\ %s\ -->

" augroup vimrc_help
"   autocmd!
"   autocmd BufEnter *.txt if &buftype == 'help' | wincmd o | endif
" augroup END

"TODO add formatoption+=a and set spell for latex automatically
" }}}

" Plugins {{{
call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-commentary'                 " Comment things
Plug 'tpope/vim-surround'                   " Surround things
Plug 'tpope/vim-unimpaired'                 " Move around
Plug 'tpope/vim-repeat'                     " Repeat things!

" Plug 'kana/vim-textobj-user'                " Fundamental plugin
" Plug 'somini/vim-textobj-fold'              " Folds are objects too!
" Plug 'kana/vim-textobj-indent'              " For indented text
" Plug 'kana/vim-textobj-underscore'          " For inside _underscores_

" Plug 'SirVer/ultisnips'                     " Snippets ultimate
Plug 'honza/vim-snippets'                   " Snippets actually

Plug 'christoomey/vim-tmux-navigator'       " Seamlessly jump between panes
Plug 'christoomey/vim-tmux-runner'          " Send text between panes

Plug 'sheerun/vim-polyglot'                 " Highlight all the filetypes
Plug 'lervag/vimtex'                        " Making LaTeX easy
" Plug 'chriskempson/base16-vim'              " Aesthetics

Plug 'airblade/vim-gitgutter'               " Now it's an IDE
Plug 'plasticboy/vim-markdown'              " More features!

Plug '/usr/local/opt/fzf'
Plug 'junegunn/fzf.vim'                     " Yeah!
Plug 'junegunn/vim-easy-align'

" Plug 'jalvesaq/Nvim-R'
call plug#end()
runtime macros/matchit.vim
"}}}

" Polyglot {{{
let g:polyglot_disabled = ['latex', 'markdown']
" }}}

" UltiSnips {{{
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
" }}}

" netrw {{{
let g:netrw_banner=0 " banner
let g:netrw_liststyle=3 " tree view
" }}}

" vim-tmux-navigator {{{
let g:tmux_navigator_no_mappings = 1

nnoremap <silent> <C-Left> :TmuxNavigateLeft<cr>
nnoremap <silent> <C-Down> :TmuxNavigateDown<cr>
nnoremap <silent> <C-Up> :TmuxNavigateUp<cr>
nnoremap <silent> <C-Right> :TmuxNavigateRight<cr>
nnoremap <silent> <C-\> :TmuxNavigatePrevious<cr>
"}}}

" vim-tmux-runner {{{
let g:VtrPercentage = 50
let g:VtrOrientation = "h"
let g:VtrStripLeadingWhitespace = 0
let g:VtrClearEmptyLines = 0
let g:VtrAppendNewline = 1
nnoremap <leader>ro :VtrOpenRunner<CR>
nnoremap <leader>ra :VtrAttachToPane<CR>
nnoremap <leader>rd :VtrSendCtrlD<CR>
nnoremap <leader>rf :%VtrSendLinesToRunner<CR>
nnoremap <leader>rr :VtrSendLinesToRunner<CR>j
vnoremap <leader>rr :VtrSendLinesToRunner<CR>'>j
nnoremap <leader>rp vip:VtrSendLinesToRunner<CR>'>j
" }}}

" vim-markdown {{{
let g:vim_markdown_folding_disabled = 1
let g:tex_conceal = ""
let g:vim_markdown_math = 1
let g:vim_markdown_auto_insert_bullets = 0
let g:vim_markdown_new_list_item_indent = 0
"}}}

"{{{ FZF
let g:fzf_tags_command = 'ctags -R'
nnoremap <leader>f :Files<CR>
nnoremap <leader>b :Buffers<CR>
nnoremap <leader>t :Tags<CR>
nnoremap <leader>o :History<CR>

autocmd! FileType fzf
autocmd  FileType fzf set laststatus=0 noshowmode nonumber norelativenumber
  \| autocmd BufLeave <buffer> set laststatus=2 showmode number relativenumber

" Customize fzf colors to match your color scheme
let g:fzf_colors =
            \ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

"}}}

" Color scheme {{{
" if filereadable(expand("~/.vimrc_background"))
"   let base16colorspace=256
"   source ~/.vimrc_background
" endif
highlight Visual cterm=None ctermbg=DarkGray ctermfg=None
highlight Search cterm=None ctermbg=DarkGray ctermfg=None
highlight IncSearch cterm=None ctermbg=DarkBlue ctermfg=None
" }}}

" minibracket {{{
" minibracket.vim - insert matching pair symbol
" Maintainer: Ivan Krukov
" Version: 0.0
" Insert a matching delimeter
" Bare minimum - the plugin is not smart about deletion or skipping over matching

function! MiniBracketClosePair(left, right) abort
    let this_char = getline('.')[col('.')-1]
    if this_char !~ '\k'
        return a:left . a:right . "\<Left>"
    else
        return a:left
    endif
endfunction

function! MiniBracketIgnoreClosing(right) abort
    let this_char = getline('.')[col('.')-1]
    if this_char =~ a:right
        return "\<Right>"
    else
        return a:right
    endif
endfunction

function! MiniBracketTypeOverClosing(right) abort

endfunction

inoremap <expr> { MiniBracketClosePair('{', '}')
inoremap <expr> [ MiniBracketClosePair('[', ']')
inoremap <expr> ( MiniBracketClosePair('(', ')')

inoremap <expr> } MiniBracketIgnoreClosing('}')
inoremap <expr> ] MiniBracketIgnoreClosing(']')
inoremap <expr> ) MiniBracketIgnoreClosing(')')
" }}}

" Easy-Align {{{
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
" }}}
