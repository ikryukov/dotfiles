PREFIX=~/miniconda3
CONDA_INSTALLER=Miniconda3-latest-Linux-x86_64.sh

.PHONY: conda

ALL: PKG CONF LAST
PKG: conda/vim conda/tmux conda/ncurses conda/git conda/zsh
CONF: ~/.vim/autoload/plug.vim ~/.oh-my-zsh ~/.vimrc ~/.tmux.conf

conda: ${PREFIX}

${PREFIX}: ${CONDA_INSTALLER}
	bash ${CONDA_INSTALLER}

${CONDA_INSTALLER}:
	curl -O https://repo.anaconda.com/miniconda/${CONDA_INSTALLER}

conda/%: conda
	${PREFIX}/bin/conda install -c conda-forge $(@F) -qy

~/.vim/autoload/plug.vim:
	curl -fLo $@ --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	${PREFIX}/bin/vim +PlugInstall +qa

~/.oh-my-zsh:
	curl -fsSLo install_zsh.sh https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh
	PATH=${PREFIX}/bin:${PATH} && bash ./install_zsh.sh --unattended
	ln -sf $(abspath zshrc) $(abspath ${HOME}/.zshrc)
	${PREFIX}/bin/conda init zsh bash
	rm install_zsh.sh

${HOME}/.%: %
	ln -s $(abspath $<) $(abspath $@)

LAST:
	@echo 'Need to do a few things by hand:'
	@echo '  > source ~/.bashrc'
	@echo 'change the default shell to zsh'
	
