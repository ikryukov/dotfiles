# vars
export PATH=$HOME/bin:/usr/local/bin:$PATH
export ZSH=$HOME/.oh-my-zsh

# aliases
alias glo='git log --all --oneline --graph'
alias gst='git status'

function mkcd() {
	mkdir $1 && cd $1
}

# oh my zsh config
ZSH_THEME="simple"
HYPHEN_INSENSITIVE="true"
plugins=(git)
source $ZSH/oh-my-zsh.sh

# FZF
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh


# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/ivan/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/ivan/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/ivan/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/ivan/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

